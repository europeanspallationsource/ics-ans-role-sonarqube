ics-ans-role-sonarqube
===================

Ansible role to install [SonarQube](https://www.sonarqube.org/).

By default the role installs the latest SonarQube LTS together
with PostgreSQL 9 as separate docker containers.

Setting the variable 'sonarqube_database_host' will configure
SonarQube to use an external database host, rather than
defaulting to creating a PostgreSQL container.
The default variable file, 'default/main.yml' also contains
commented variables for setting up MySQL instead of PostgreSQL.


Requirements
------------

- ansible >= 2.3
- molecule >= 1.24

Role Variables
--------------

```yaml
sonarqube_image: sonarqube:lts
sonarqube_container_name: sonarqube
sonarqube_network: sonarqube-network
sonarqube_hostnames:
  - "{{ ansible_fqdn }}"
sonarqube_port: 9000
sonarqube_dir: /var/sonarqube
sonarqube_conf_dir: "{{ sonarqube_dir }}/conf"
sonarqube_data_dir: "{{ sonarqube_dir }}/data"
sonarqube_extensions_dir: "{{ sonarqube_dir }}/extensions"
sonarqube_plugins_dir: "{{ sonarqube_extensions_dir }}/plugins"
sonarqube_plugins: []

sonarqube_security_realm:
sonarqube_ldap_url:
sonarqube_ldap_bind_dn:
sonarqube_ldap_bind_password:
sonarqube_ldap_auth: simple
sonarqube_ldap_starttls: false
sonarqube_ldap_followreferrals: true
sonarqube_ldap_user_base:
sonarqube_ldap_user_request:
sonarqube_ldap_user_attribute_realname: cn
sonarqube_ldap_user_attribute_email: mail
sonarqube_ldap_group_base:
sonarqube_ldap_group_request:
sonarqube_ldap_group_attribute_id: cn

sonarqube_database_container_name: sonarqube_database
sonarqube_database_name: sonarqube
sonarqube_database_username: sonarqube
sonarqube_database_password: sonarqube
sonarqube_database_dir: "{{ sonarqube_dir }}/database"

# PostgreSQL
sonarqube_database_image: postgres:9
sonarqube_database_url: "jdbc:postgresql://{{ sonarqube_database_host }}:{{ sonarqube_database_port }}/{{ sonarqube_database_name }}"
sonarqube_database_port: 5432
sonarqube_database_env:
  POSTGRES_USER: "{{ sonarqube_database_username }}"
  POSTGRES_PASSWORD: "{{ sonarqube_database_password }}"
  POSTGRES_DB: "{{ sonarqube_database_name }}"
```

Example Playbook
----------------

```yaml
- hosts: servers
  roles:
    - role: ics-ans-role-sonarqube
```

License
-------

BSD 2-clause
