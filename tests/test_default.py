import testinfra.utils.ansible_runner
import json

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    '.molecule/ansible_inventory').get_hosts('all')


def test_sonarqube_containers(Command, Sudo):
    with Sudo():
        cmd = Command("set +x; docker ps --format \{\{.Names\}\}")
    assert cmd.rc == 0
    # Get the names of the running containers
    names = cmd.stdout.splitlines()
    print names
    assert all(name in names for name in [u'sonarqube', u'sonarqube_database', u'traefik_proxy'])


def test_sonarqube_application(Command):
    # This tests that traefik forwards traffic to the SonarQube web server
    # and that we can access the SonarQube application
    cmd = Command('curl -k https://ics-ans-role-sonarqube-default/')
    assert '<title>SonarQube</title>' in cmd.stdout


def test_sonarqube_api_status(Command):
    # This tests that the SonarQube API is functioning and that the system
    # status is "UP"
    cmd = Command('curl -k https://ics-ans-role-sonarqube-default/api/system/status')
    print cmd.stdout
    status = json.loads(cmd.stdout)
    assert status['status'] == "UP"
